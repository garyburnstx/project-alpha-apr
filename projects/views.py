from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from tasks.models import Task
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "project_list": projects,
    }
    return render(request, "projects/project_list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    task = Task.objects.filter(project=project)
    context = {
        "project": project,
        "task": task,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    return render(request, "projects/create_project.html", {"form": form})
